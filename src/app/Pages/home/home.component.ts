import {Component} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  ngOnInit(): void {
    // @ts-ignore
    document.getElementById("transition").style.display = "block";
  }

  goToMenu() {
    setTimeout(() => {
      // @ts-ignore
      document.getElementById("transition").style.display = "none";
    }, 1000);

  }
}

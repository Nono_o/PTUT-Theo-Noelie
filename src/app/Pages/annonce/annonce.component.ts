import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-annonce',
  templateUrl: './annonce.component.html',
  styleUrls: ['./annonce.component.css'],
})
export class AnnonceComponent {

  constructor(private router: Router) {
  }

  ngOnInit(): void {
// @ts-ignore
    document.getElementById("transition").style.display = "none";
    // @ts-ignore
    document.getElementById("fadeIn").style.display = "block";
    setTimeout(() => {
      // @ts-ignore
      document.getElementById("fadeIn").style.display = "none";
    }, 3000);
  }

  goToMenu() {
    // @ts-ignore
    document.getElementById("transition").style.display = "block";
    setTimeout(() => {

      this.router.navigate(['/home']);
    }, 1000);

  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Components/header/header.component';
import { FooterComponent } from './Components/footer/footer.component';
import { MainMenuCardComponent } from './Components/main-menu-card/main-menu-card.component';
import { TextIntroComponent } from './Components/text-intro/text-intro.component';
import { ArticleComponent } from './Components/article/article.component';
import { HomeComponent } from './Pages/home/home.component';
import { AnnonceComponent } from './Pages/annonce/annonce.component';
import { LoginComponent } from './Pages/login/login.component';
import { RegisterComponent } from './Pages/register/register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainMenuCardComponent,
    TextIntroComponent,
    ArticleComponent,
    HomeComponent,
    AnnonceComponent,
    LoginComponent,
    RegisterComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }



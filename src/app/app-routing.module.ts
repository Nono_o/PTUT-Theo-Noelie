import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./Pages/home/home.component";
import {AnnonceComponent} from "./Pages/annonce/annonce.component";
import {LoginComponent} from "./Pages/login/login.component";
import { RegisterComponent } from './Pages/register/register.component';



const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: '', component: AnnonceComponent},
  {path:'accueil', component: AnnonceComponent},
  {path:'connexion', component: LoginComponent},
  {path:'inscription', component: RegisterComponent},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})



export class AppRoutingModule {


}
